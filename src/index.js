import React from 'react';
import { render } from 'react-dom';
import {
    Router,
    Route,
    Switch,
} from 'react-router-dom';
import createBrowserHistory from "history/createBrowserHistory";
import Welcome from './scenes/Welcome';

import './index.scss';

const history = createBrowserHistory()

render(
    <Router history={history}>
        <Switch>
            <Route path="/" component={Welcome} />
        </Switch>
    </Router>,
    document.getElementById('root')
);
