import React from 'react';
import style from './style.module.scss';

class Content extends React.Component {

  render() {
    return (
      <section className={style.content}>
        {this.props.children}
      </section>
    );
  }
}

export default Content;
