import React, { Component } from 'react';
import Header from 'Components/Header';
import Sidebar from 'Components/Sidebar';
import Content from 'Components/Content';

import styles from './styles.module.scss';

class Welcome extends Component {

  render() {
    return (
      <React.Fragment>
        <Header />
        <Sidebar />
        <Content>
          <div className={styles.wrapper}>Here's some content!</div>
        </Content>
      </React.Fragment>
    );
  }
}

export default Welcome;
